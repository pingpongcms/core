<?php

namespace Pingpongcms\Core\Seeders;

use Illuminate\Database\Seeder;
use Pingpongcms\Users\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        User::create([
            'name' => 'Administrator',
            'username' => 'admin',
            'email' => 'pingpong.labs@gmail.com',
            'password' => 'secret',
        ]);
    }
}
