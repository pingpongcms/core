<?php

namespace Pingpongcms\Core\Seeders;

use Illuminate\Database\Seeder;
use Pingpongcms\Terms\Term;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        Term::create([
            'type' => 'category',
            'name' => 'Uncategorized',
            'slug' => 'uncategorized',
            'description' => 'Uncategorized',
        ]);
    }
}