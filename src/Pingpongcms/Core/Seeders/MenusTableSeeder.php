<?php

namespace Pingpongcms\Core\Seeders;

use Illuminate\Database\Seeder;
use Pingpongcms\Menus\Menu;
use Pingpongcms\Menus\MenuItem;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::truncate();
        MenuItem::truncate();

        // frontend
        $menu = Menu::make('main', 'The main menu.');

        $menu->addItem('Home', '/');
        $menu->addItem('About', '/about');
        $menu->addItem('Posts', '/posts');
        $menu->addItem('Contact', '/contact');

        // dashboard
        $menu = Menu::make('dashboard', 'The dashboard menu.');

        $menu->addItem('Home', '/admin');
        
        $item = $menu->addItem('Users', '/admin/users', 'index-user|create-user');
        $item->addItem('Add New', '/admin/users/create', 'create-user');
        $item->addItem('All Users', '/admin/users', 'index-user');

        $item = $menu->addItem('Posts', '/admin/posts', 'index-post|create-post|index-comment|index-category|index-tag');
        $item->addItem('Add New', '/admin/posts/create', 'create-post');
        $item->addItem('All Posts', '/admin/posts', 'index-post');
        $item->addDivider(['permission' => 'index-post|create-post']);
        $item->addItem('Categories', '/admin/terms?type=category', 'index-category');
        $item->addItem('Tags', '/admin/terms?type=tag', 'index-tag');
        $item->addDivider(['permission' => 'index-category|index-tag']);
        $item->addItem('Comments', '/admin/comments', 'index-comment');

        $item = $menu->addItem('Pages', '/admin/posts?type=page', 'index-page|create-page');
        $item->addItem('Add New', '/admin/posts/create?type=page', 'create-page');
        $item->addItem('All Pages', '/admin/posts?type=page', 'index-page');

        $item = $menu->addItem('Account', '#!');
        $item->addHeader('SETTINGS', ['permission' => 'update-settings|index-menu|index-role|index-permission|see-logs']);
        $item->addItem('General', '/admin/settings', 'update-settings');
        $item->addItem('Menus', '/admin/menus', 'index-menu');
        $item->addItem('Themes', '/admin/themes', 'update-theme_current');
        $item->addItem('Roles', '/admin/roles', 'index-role');
        $item->addItem('Permissions', '/admin/permissions', 'index-permission');
        $item->addItem('Logs', '/admin/logs', 'see-logs');
        $item->addDivider();
        $item->addItem('Logout', '/logout');
    }
}
