<?php

namespace Pingpongcms\Core\Seeders;

use Illuminate\Database\Seeder;
use Pingpongcms\Users\Permission;
use Pingpongcms\Users\Role;
use Pingpongcms\Users\User;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $roles = [
            'Super Admin',
            'Admin',
            'User',
        ];

        foreach ($roles as $role) {
            Role::create([
                'name' => str_slug($role),
                'description' => $role,
            ]);
        }

        $permissions = config('permissions', []);

        $settings = collect(array_keys(config('settings', [])))->map(function ($item)
        {
            return 'update-'.$item;
        })->toArray();

        $permissions = array_merge($settings, $permissions);

        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission,
                'description' => ucwords(str_replace('-', ' ', $permission)),
            ]);
        }

        $superAdmin = 'super-admin';
        // add role as super admin to first user
        User::firstOrFail()->addRole($superAdmin);
        // add all permissions to super admin
        Role::whereName($superAdmin)
            ->firstOrFail()
            ->addPermission($permissions);

        // add permissions for admin
        Role::whereName('admin')
            ->firstOrFail()
            ->addPermission(array_slice($permissions, 6));
    }
}
