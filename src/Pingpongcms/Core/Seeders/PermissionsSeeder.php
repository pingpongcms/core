<?php

namespace Pingpongcms\Core\Seeders;

use Illuminate\Database\Seeder;
use Pingpongcms\Users\Permission;
use Pingpongcms\Users\Role;

abstract class PermissionsSeeder extends Seeder
{
    protected $permissions = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->permissions as $name => $description) {
            $permission = Permission::create(compact('name', 'description'));
            
            $roles = Role::whereName('admin')->orWhere('name', 'super-admin')->get();
            
            foreach ($roles as $role) {
                $role->permissions()->attach($permission);
            }
        }
    }
}
