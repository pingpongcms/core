<?php

namespace Pingpongcms\Core\Seeders;

use Illuminate\Database\Seeder;
use Pingpongcms\Settings\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Setting::truncate();

        foreach (config('settings', []) as $key => $value) {
            Setting::create(compact('key', 'value'));
        }
    }
}
