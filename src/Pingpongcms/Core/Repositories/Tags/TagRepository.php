<?php

namespace Pingpongcms\Core\Repositories\Tags;

use Pingpongcms\Terms\Repositories\TermRepository;

class TagRepository extends TermRepository
{
    public function type()
    {
        return 'tag';
    }
}