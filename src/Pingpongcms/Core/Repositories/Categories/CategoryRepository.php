<?php

namespace Pingpongcms\Core\Repositories\Categories;

use Pingpongcms\Terms\Repositories\TermRepository;

class CategoryRepository extends TermRepository
{
    public function type()
    {
        return 'tag';
    }
}