<?php

namespace Pingpongcms\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Pingpongcms\Core\Http\Composers\ShareCategories;
use Pingpongcms\Core\Http\Composers\ShareTags;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $config = [
            'settings' => __DIR__.'/../../../config/settings.php',
            'permissions' => __DIR__.'/../../../config/permissions.php',
            'validation' => __DIR__.'/../../../config/validation.php',
            'core' => __DIR__.'/../../../config/core.php',
        ];

        $this->publishes([
            $config['settings'] => config_path('settings.php'),
            $config['permissions'] => config_path('permissions.php'),
            $config['validation'] => config_path('validation.php'),
            $config['core'] => config_path('core.php'),
        ]);

        $this->mergeConfigFrom($config['settings'], 'settings');
        $this->mergeConfigFrom($config['permissions'], 'permissions');
        $this->mergeConfigFrom($config['validation'], 'validation');
        $this->mergeConfigFrom($config['core'], 'core');

        $this->publishes([
            __DIR__.'/../../../resources/views/' => base_path('resources/views')
        ], 'views');

        $this->app['view']->addLocation(__DIR__.'/../../../resources/views');

        require __DIR__ . '/../settings.php';

        $this->registerViewComposers();
    }

    public function registerViewComposers()
    {
        $this->app['view']->composer('layouts._sidebar', ShareCategories::class);
        $this->app['view']->composer('layouts._sidebar', ShareTags::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
