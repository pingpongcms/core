<?php

$tabs = app('settings.tabs');

$tabs->add('Site', function ($tab) {
    $tab
        ->addSetting('Site Name:', 'site_name', 'text')
        ->addSetting('Site Slogan:', 'site_slogan', 'text')
        ->addSetting('Site Description:', 'site_description', 'textarea')
        ->addSetting('Site Keywords:', 'site_keywords', 'text')
        ->addSetting('Site Author:', 'site_author', 'text')
        ->addSetting('Site Prefix URL:', 'site_prefix', 'text')
    ;
})->order(1);

$tabs->add('Analytics', function ($tab) {
    $tab
        ->addSetting('Google Analytics Code:', 'ga_code', 'text')
    ;
})->order(6);

$tabs->add('Authentication', function ($tab) {
    $tab->addSetting('Enable Registration:', 'register_enabled', function () {
        $options = [
            1 => 'Yes',
            0 => 'No'
        ];
        return Form::select("settings[register_enabled]", $options, setting('register_enabled'), ['class' => 'form-control']);
    });
})->order(6);

View::composer('settings::admin.index', function ($view) {
    $tabs = app('settings.tabs')->getOrdered();

    $view->with(compact('tabs'));
});
