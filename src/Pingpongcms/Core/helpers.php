<?php

if (!function_exists('tab_active')) {
    function tab_active($status, $current, $class = 'active')
    {
        return $status == $current ? $class : null;
    }
}

if (!function_exists('site_title')) {
    function site_title()
    {
        $siteName = setting('site_name');
        $siteSlogan = setting('site_slogan');

        $title = View::yieldContent('title');

        return $title ? "{$title} | {$siteName}" : "{$siteName} | {$siteSlogan}"; 
    }
}