<?php

Route::group(['middleware' => 'web', 'prefix' => setting('site_prefix', '/')], function () {

    if (! config('core.disable_auth_routes')) {
        require __DIR__ . '/Routes/auth.php';
    }

    // Frontend Routes
    Route::get('/', [
        'as' => 'home',
        'uses' => 'HomeController@index',
    ]);
    Route::get('category/{slug}', [
        'as' => 'posts.category',
        'uses' => 'PostsController@byCategory',
    ]);
    Route::get('tag/{slug}', [
        'as' => 'posts.tag',
        'uses' => 'PostsController@byTag',
    ]);
    Route::get('author/{username}', [
        'as' => 'posts.author',
        'uses' => 'PostsController@byAuthor',
    ]);
    Route::get('posts', [
        'as' => 'posts.index',
        'uses' => 'PostsController@index',
    ]);
    Route::get('search', [
        'as' => 'search',
        'uses' => 'SearchController@index',
    ]);

    Route::get('posts/{slug}', [
        'as' => 'posts.show',
        'uses' => 'PostsController@show',
    ]);
    Route::post('posts/{slug}', [
        'as' => 'posts.comment',
        'uses' => 'PostsController@comment',
    ]);
});

App::booted(function ($app)
{
    Route::group(['middleware' => 'web', 'prefix' => setting('site_prefix', '/')], function () {
        Route::get('{slug}', [
            'as' => 'pages.show',
            'uses' => 'Pingpongcms\Core\Http\Controllers\PagesController@show',
        ]);
    });
});