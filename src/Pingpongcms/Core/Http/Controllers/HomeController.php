<?php

namespace Pingpongcms\Core\Http\Controllers;

use Pingpongcms\Posts\Post;

class HomeController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->paginate(10);

        return view('welcome', compact('posts'));
    }
}
