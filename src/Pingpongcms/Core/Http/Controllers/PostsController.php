<?php

namespace Pingpongcms\Core\Http\Controllers;

use Pingpongcms\Core\Http\Requests\CreateCommentRequest;
use Pingpongcms\Core\Repositories\Categories\CategoryRepository;
use Pingpongcms\Core\Repositories\Tags\TagRepository;
use Pingpongcms\Posts\Repositories\PostRepository;
use Pingpongcms\Users\Repositories\UserRepository;

class PostsController extends Controller
{
    private $posts;

    private $users;
    
    private $tags;

    private $categories;

    public function __construct(PostRepository $posts, UserRepository $users, TagRepository $tags, CategoryRepository $categories)
    {
        $this->posts = $posts;
        $this->users = $users;
        $this->tags = $tags;
        $this->categories = $categories;
    }

    public function index()
    {
        $posts = $this->posts->getLatestPaginated();

        return view('posts.index', compact('posts'))
            ->with([
                'title' => "All Posts ({$posts->total()})",
                'description' => 'All the latest posts.',
            ])
        ;
    }

    public function show($slug)
    {
        $post = $this->posts->find($slug, ['user', 'categories', 'tags', 'metas']);

        $eventClass = class_exists('App\Events\PostShowEvent')
            ? \App\Events\PostShowEvent::class
            : \Pingpongcms\Core\Events\PostShowEvent::class;

        event(new $eventClass($post));

        $comments = $this->posts->getCommentsByPost($post);

        $defaultTemplate = 'posts.show';

        $template = $post->meta('template', $defaultTemplate);

        if (! view()->exists($template)) {
            $template = $defaultTemplate;
        }

        return view($template, compact('post', 'comments'));
    }

    public function byAuthor($author)
    {
        $user = $this->users->find($author);

        $posts = $this->posts->getByRelation($user);

        return view('posts.index', compact('user', 'posts', 'author'))
            ->with([
                'title' => "Posts by {$user->name} ({$posts->total()})",
                'description' => "All the latest posts from {$user->name}.",
            ])
        ;
    }

    public function byTag($tag)
    {
        $tag = $this->tags->find($tag);

        $posts = $this->posts->getByRelation($tag);

        return view('posts.index', compact('tag', 'posts'))
            ->with([
                'title' => "Tag: {$tag->name} ({$posts->total()})",
                'description' => "All posts with tag {$tag->name}.",
            ])
        ;
    }

    public function byCategory($category)
    {
        $category = $this->categories->find($category);

        $posts = $this->posts->getByRelation($category);

        return view('posts.index', compact('category', 'posts'))
            ->with([
                'title' => "Category: {$category->name} ({$posts->total()})",
                'description' => "All posts with category {$category->name}.",
            ])
        ;
    }

    public function comment(CreateCommentRequest $request, $slug)
    {
        $post = $this->posts->find($slug);

        $post->comments()->create([
            'user_id' => auth()->check() ? auth()->id() : 0,
            'body' => $request->comment,
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
            'notify' => $request->get('notify', 0),
        ]);

        return redirect()->back();
    }
}
