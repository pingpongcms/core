<?php

namespace Pingpongcms\Core\Http\Controllers;

use Illuminate\Http\Request;
use Pingpongcms\Posts\Repositories\PostRepository;

class SearchController extends Controller
{
    private $posts;

    public function __construct(PostRepository $posts)
    {
        $this->posts = $posts;
    }

    public function index(Request $request)
    {
        $q = $request->q;

        $search = "%{$q}%";

        $posts = $this->posts->search($search);

        return view('posts.index', compact('posts', 'q'))
            ->with([
                'title' => 'Search',
                'description' => 'Search results for "'.$q.'"',
            ])
        ;
    }
}
