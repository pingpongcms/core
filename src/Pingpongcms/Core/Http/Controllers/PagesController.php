<?php

namespace Pingpongcms\Core\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Pingpongcms\Posts\Repositories\PostRepository;

class PagesController extends Controller
{
    private $posts;

    public function __construct(PostRepository $posts)
    {
        $this->posts = $posts;
    }

    public function show($slug)
    {
        $post = $this->posts->find($slug, ['user', 'categories', 'tags', 'metas']);

        $comments = $this->posts->getCommentsByPost($post);

        $defaultView = 'pages.show';

        $view = $post->meta('template', $defaultView);

        if (! view()->exists($view)) {
            $view = $defaultView;
        }

        return view($view, compact('post', 'comments'));
    }
}
