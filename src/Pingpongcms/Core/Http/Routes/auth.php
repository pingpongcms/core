<?php

// Authentication Routes...
Route::get('login', [
    'as' => 'login', 
    'uses' => 'Auth\AuthController@showLoginForm'
]);
Route::post('login', [
    'as' => 'login', 
    'uses' => 'Auth\AuthController@login'
]);
Route::get('logout', [
    'as' => 'logout', 
    'uses' => 'Auth\AuthController@logout'
]);

// Registration Routes...
Route::get('register', [
    'as' => 'register', 
    'uses' => 'Auth\AuthController@showRegistrationForm'
]);
Route::post('register', [
    'as' => 'register', 
    'uses' => 'Auth\AuthController@register'
]);

// Password Reset Routes...
Route::get('password/reset/{token?}', [
    'as' => 'password.reset', 
    'uses' => 'Auth\PasswordController@showResetForm'
]);
Route::get('password/email', [
    'as' => 'password.email', 
    'uses' => 'Auth\PasswordController@showLinkRequestForm'
]);
Route::post('password/email', [
    'as' => 'password.email', 
    'uses' => 'Auth\PasswordController@sendResetLinkEmail'
]);
Route::post('password/reset/{token?}', [
    'as' => 'password.reset',
    'uses' => 'Auth\PasswordController@reset'
]);