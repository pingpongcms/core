<?php

namespace Pingpongcms\Core\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class CreateCommentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'comment' => 'required|max:1000',
        ];

        if (!Auth::check()) {
            $rules = array_merge($rules, [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255',
                'website' => 'required|url|max:255',
            ]);
        }

        return $rules;
    }
}
