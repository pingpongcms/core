<?php

namespace Pingpongcms\Core\Http\Composers;

use Pingpongcms\Terms\Term;

class ShareCategories
{
    public function compose($view)
    {
        $categories = Term::categories()->get();
        
        $view->with(compact('categories'));   
    }    
}