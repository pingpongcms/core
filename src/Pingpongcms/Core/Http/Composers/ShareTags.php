<?php

namespace Pingpongcms\Core\Http\Composers;

use Pingpongcms\Terms\Term;

class ShareTags
{
    public function compose($view)
    {
        $tags = Term::tags()->get();

        $view->with(compact('tags'));   
    }    
}