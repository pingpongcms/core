@extends('layouts.master')

@include('layouts._meta')

@section('header')
    <div class="container-fluid content-header">
        <div class="container">
            <h1>{{{ $title or "Posts" }}}</h1>
            <div class="text-muted">
                {{{ $description or "" }}}
            </div>
        </div>
    </div>
@stop

@section('content')
    
    @include('posts._list')

@stop