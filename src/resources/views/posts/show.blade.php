@extends('layouts.master')

@include('layouts._meta', [
    'title' => $post->title,
    'description' => $post->excerpt,
])

@section('header')
    <div class="container-fluid content-header">
        <div class="container">
            <h1>{{ $post->title }}</h1>
            <div class="text-muted">
                Published at {{ $post->published_at->formatLocalized('%d %B %Y') }}
                by <a href="{{ route('posts.author', $post->user->username) }}">{{ $post->user->name }}</a>
                On
                @foreach ($post->categories as $category)
                    <a class="label label-default" href="{{ route('posts.category', $category->slug) }}">
                        {{ $category->name }}
                    </a>
                    &nbsp;
                @endforeach
            </div>
        </div>
    </div>
@stop

@section('content')
    
    <div class="media-image">
        {!! $post->image(['class' => 'img-responsive']) !!}
    </div>
    <article>
        {!! $post->content !!}
    </article>
    <div class="post-tags">
        Tags:
        @foreach ($post->tags as $tag)
            <a href="{{ route('posts.tag', $tag->slug) }}" class="label label-default">
                {{ $tag->name }}
            </a>
            &nbsp;
        @endforeach 
    </div>

    @if (setting('comment_enabled'))
    <div id="comments" class="row col-md-10">
        <h3 class="page-header">{{ $comments->total() }} Comments</h3>
        @foreach ($comments as $comment)
            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="{{ $comment->avatar_url }}" alt="{{ $comment->user_name }}">
                </a>
                <div class="media-body">
                    <h4 class="media-heading">
                        {{ $comment->user_name }}
                        <small class="text-muted pull-right">{{ $comment->created_at->diffForHumans() }}</small>
                    </h4>
                    <p>{{ $comment->body }}</p>
                </div>
            </div>
        @endforeach
        <hr>
        {!! Form::open(['route' => ['posts.comment', $post->slug]]) !!}
            @if (!Auth::check())
                    
            <div class="form-group">
                <label for="name">Name:</label>
                {!! Form::text('name', null, ['required', 'class' => 'form-control']) !!}
                {!! error_for('name', $errors) !!}
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                {!! Form::email('email', null, ['required', 'class' => 'form-control']) !!}
                {!! error_for('email', $errors) !!}
            </div>
            <div class="form-group">
                <label for="website">Website:</label>
                {!! Form::url('website', null, ['required', 'class' => 'form-control']) !!}
                {!! error_for('website', $errors) !!}
            </div>
            @endif
            <div class="form-group">
                <label for="comment">Comment:</label>
                {!! Form::textarea('comment', null, ['required', 'rows' => 5, 'class' => 'form-control']) !!}
                {!! error_for('comment', $errors) !!}
            </div>
            <div class="form-group">
                <label>
                    {!! Form::checkbox('notify', 1) !!}
                    Notify me by email.
                </label>
            </div>
            <div class="form-group">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            </div>
        {!! Form::close() !!}
    </div>
    @endif

@stop