@forelse ($posts as $post)
    <div class="media">
        <div class="media-body">
            <h4 class="media-heading">
                <a href="{{ route('posts.show', $post->slug) }}">
                    {{ $post->title }}
                </a>
            </h4>
            <nav class="text-muted">
                Published at {{ $post->published_at->formatLocalized('%d %B %Y') }}
                by <a href="{{ route('posts.author', $post->user->username) }}">{{ $post->user->name }}</a>
            </nav>
            <a href="{{ route('posts.show', $post->slug) }}">
                {!! $post->image(['class' => 'img-responsive']) !!}
            </a>
            <p>{{ $post->excerpt }}</p>
        </div>
    </div>
@empty
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>No Posts!</strong>
        There are no posts.
    </div>
@endforelse

<div class="text-center">
    {!! $posts->render() !!}
</div>