@extends('layouts.master')

@section('content')
    
    <h3 class="page-header">Register</h3>

    @include('errors.validation')
    
    {!! Form::open() !!}
        <div class="form-group">
            <label for="name">Name:</label>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <label for="username">Username:</label>
            {!! Form::text('username', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <label for="email">Email:</label>
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <label for="password">Password:</label>
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <label for="password_confirmation">Password Confirmation:</label>
            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Register', ['class' => 'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}

    <hr>

    <a href="{{ route('login') }}">Login</a><br>
    <a href="{{ route('password.email') }}">Forgot Password</a>

@stop