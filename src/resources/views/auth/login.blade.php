@extends('layouts.master')

@section('content')
    
    {!! Form::open() !!}
        <h3 class="page-header">Login</h3>
        <div class="form-group">
            <label for="email">Email:</label>
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <label for="password">Password:</label>
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <label>
                {!! Form::checkbox('remember', 1) !!}
                Remember me
            </label>
        </div>
        <div class="form-group">
            {!! Form::submit('Login', ['class' => 'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}

    <hr>

    @if (setting('register_enabled'))
        <a href="{{ route('register') }}">Register</a><br>
    @endif

    <a href="{{ route('password.email') }}">Forgot Password</a>

@stop