@extends('layouts.master')

@section('content')
    
    <h3 class="page-header">Forgot Password</h3>
    
    @include('errors.validation')

    {!! Form::open() !!}
        <div class="form-group">
            <label for="email">Email:</label>
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Send Reminder Email', ['class' => 'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}

    <hr>

    <a href="{{ route('login') }}">Login</a><br>
    <a href="{{ route('register') }}">Register</a><br>

@stop