@extends('layouts.master')

@section('content')
    
    <h3 class="page-header">Reset Password</h3>
    
    @include('errors.validation')

    {!! Form::open() !!}
        {!! Form::hidden('token', $token) !!}
        <div class="form-group">
            <label for="email">Email:</label>
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <label for="password">Password:</label>
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <label for="password_confirmation">Password Confirmation:</label>
            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Reset Password', ['class' => 'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}

@stop