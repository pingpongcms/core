@extends('layouts.master')

@section('content')
    
    <h1 class="page-header">Latest Posts ({{ $posts->total() }})</h1>

    @include('posts._list')
    
@stop