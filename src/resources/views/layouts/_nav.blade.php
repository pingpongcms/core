<nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">        
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('home') }}">
                {!! setting('site_name') !!}
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            @include('menus::default', ['menu' => 'main'])
            <form action="{{ route('search') }}" method="get" class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input required type="text" class="form-control" name="q" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Go!</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                @if ( ! Auth::check())
                    <li><a href="{{ route('login') }}">Login</a></li>
                    @if (setting('register_enabled'))
                    <li><a href="{{ route('register') }}">Register</a></li>
                    @endif
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('admin.home') }}">Goto Dashboard</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ route('logout') }}">Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>