<div class="panel panel-default">
    <div class="panel-heading">
        Categories ({{ $categories->count() }})
    </div>
    <div class="list-group">
        @foreach ($categories as $category)
            <a href="{{ route('posts.category', $category->slug) }}" class="list-group-item">
                {{ $category->name }}
                <span class="badge badge-default">{{ $category->posts->count() }}</span>
            </a>
        @endforeach
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        Tags ({{ $tags->count() }})
    </div>
    <div class="panel-body">
        @foreach ($tags as $tag)
            <a href="{{ route('posts.tag', $tag->slug) }}" class="label label-default">
                {{ $tag->name }}
                <span class="badge-count">({{ $tag->posts->count() }})</span>
            </a>&nbsp;
        @endforeach
    </div>
</div>