<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ site_title() }}</title>
        <meta name="description" content="@yield('description', setting('site_description'))">
        <meta name="author" content="@yield('author', setting('site_author'))">
        <meta name="keywords" content="@yield('keywords', setting('site_keywords'))">
        
        <!-- Bootstrap CSS -->
        <link href="{{ asset('css/all.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        @yield('style')
    </head>
    <body>
        @include('layouts._nav')
        
        @yield('header')

        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @yield('content')
                </div>
                <div class="col-md-4">
                    @if (request()->is('/'))
                        <h1></h1>
                    @endif
                    @if (!request()->is('auth/*') && !request()->is('password/*'))
                        @include('layouts._sidebar')
                    @endif
                </div>
            </div>
        </div>

        <footer class="container">
            <hr>
            Copyright &copy; {{ date('Y') }}. All rights reserved.
        </footer>

        <script src="{{ asset('js/all.js') }}"></script>
        @yield('script')

        @if ($GACode = setting("ga_code"))
            <!-- Google Analytics -->
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

              ga('create', '{{ $GACode }}', 'auto');
              ga('send', 'pageview');

            </script>
        @endif
    </body>
</html>