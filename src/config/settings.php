<?php

return [
    'site_name' => 'Pingpong CMS 1.0-dev',
    'site_slogan' => 'Just another laravel CMS.',
    'site_description' => 'A Modern CMS built with Laravel PHP Framework.',
    'site_keywords' => 'laravel, starter kit, starter site, cms',
    'site_author' => 'Gravitano, Pingpong Labs',

    'post_perpage' => 10,
    'post_editor'  => 'ckeditor', // supported: ckeditor, markdown 

    'comment_perpage' => 10,
    'comment_enabled' => 1, // 1: true, 0: false
    
    'page_comment' => 0, // 1: true, 0: false

    'dashboard_prefix' => 'admin',
    'dashboard_title' => 'Administrator',
    
    'site_prefix' => '/',

    'ga_code' => '',

    'theme_current' => 'default',

    'register_enabled' => 1,

    'user_default_role' => 3,
];