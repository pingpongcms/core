<?php

return [
	
	'register' => [
		'rules' => [
	        'name' => 'required|max:255',
	        'username' => 'required|max:255|alpha_num|unique:users',
	        'email' => 'required|email|max:255|unique:users',
	        'password' => 'required|confirmed|min:6',
	    ],
	    'messages' => [

	    ]
    ]

];