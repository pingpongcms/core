<?php

return [
    'index-user',
    'show-user',
    'create-user',
    'update-user',
    'destroy-user',

    'index-role',
    'show-role',
    'create-role',
    'update-role',
    'destroy-role',

    'index-permission',
    'show-permission',
    'create-permission',
    'update-permission',
    'destroy-permission',

    'index-post',
    'show-post',
    'create-post',
    'update-post',
    'destroy-post',

    'index-page',
    'show-page',
    'create-page',
    'update-page',
    'destroy-page',

    'index-category',
    'show-category',
    'create-category',
    'update-category',
    'destroy-category',

    'index-tag',
    'show-tag',
    'create-tag',
    'update-tag',
    'destroy-tag',

    'index-comment',
    'show-comment',
    'create-comment',
    'update-comment',
    'destroy-comment',

    'index-menu',
    'show-menu',
    'create-menu',
    'update-menu',
    'destroy-menu',

    'update-settings',

    'see-logs',
    'clear-logs',
];