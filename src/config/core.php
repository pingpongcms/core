<?php

return [

	/**
	 * Disable default authentication routes.
	 */
	'disable_auth_routes' => 0

];